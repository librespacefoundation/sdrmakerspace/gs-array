INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_GSARRAY gsarray)

FIND_PATH(
    GSARRAY_INCLUDE_DIRS
    NAMES gsarray/api.h
    HINTS $ENV{GSARRAY_DIR}/include
        ${PC_GSARRAY_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GSARRAY_LIBRARIES
    NAMES gnuradio-gsarray
    HINTS $ENV{GSARRAY_DIR}/lib
        ${PC_GSARRAY_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gsarrayTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GSARRAY DEFAULT_MSG GSARRAY_LIBRARIES GSARRAY_INCLUDE_DIRS)
MARK_AS_ADVANCED(GSARRAY_LIBRARIES GSARRAY_INCLUDE_DIRS)
