/* -*- c++ -*- */
/*
 * Copyright 2019 gr-gsarray author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gsarray/algorithm.h>

namespace gr
{
namespace gsarray
{
using namespace arma;

algorithm::algorithm (size_t num_streams, uint8_t array_t, double spacing,
                      double lambda) :
        d_num_streams (num_streams),
        d_array_t (array_t),
        d_spacing (spacing),
        d_lambda (lambda),
        d_azimuth (0),
        d_elevation (0)
{
  d_positions.resize (d_num_streams);

  double lim;
  int index;
  switch (d_array_t)
    {
    case 0: /* Linear array */
      lim = (d_num_streams - 1) / 2.0;
      index = 0;

      for (float i = -lim; i <= lim; i++) {
        d_positions[index].resize (2);
        d_positions[index][0] = i * d_spacing;
        index++;
      }
      break;
    case 1: /* Square array */
      int dimension = std::sqrt (d_num_streams);
      if (!dimension * dimension == d_num_streams) {
        printf ("Number of streams given not appropriate for square array\n");
        break;
      }
      lim = (dimension - 1) / 2.0;
      index = 0;

      for (float j = -lim; j <= lim; j++) {
        for (float i = -lim; i <= lim; i++) {
          d_positions[index].resize (2);
          d_positions[index][0] = i * d_spacing;
          d_positions[index][1] = j * d_spacing;
          index++;
        }
      }
      break;
    }
  d_resp = cx_fvec (4);
  d_resp (0) = 1;
  d_resp (1) = 1;
  d_resp (2) = 1;
  d_resp (3) = 1;
}

algorithm::~algorithm ()
{
}

size_t
algorithm::beamform (int noutput_items, gr_vector_const_void_star &input_items,
                     gr_vector_void_star &output_items)
{
  return 0;
}

void
algorithm::update_coordinates (double azimuth, double elevation)
{

  d_azimuth = azimuth;
  d_elevation = elevation;
  return;
}

} /* namespace gsarray */
} /* namespace gr */

