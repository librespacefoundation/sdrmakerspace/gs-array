#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <cmath>
#include <complex>
#include <math.h>
#include "collector_impl.h"
#include <iostream>
#include <fstream>

namespace gr
{
namespace gsarray
{

collector::sptr
collector::make (uint16_t num_elements, float dist, float lambda,
                 uint8_t array_conf, uint8_t input_ang_src, float ang_az, float ang_el)
{
  return gnuradio::get_initial_sptr (
      new collector_impl (num_elements, dist, lambda, array_conf, input_ang_src, ang_az, ang_el));
}

/*
 * The private constructor
 */
collector_impl::collector_impl (uint16_t num_elements, float dist, float lambda,
                                uint8_t array_conf, uint8_t input_ang_src, float ang_az, float ang_el) :
        gr::sync_block ("collector",
                        gr::io_signature::make (num_elements, num_elements, sizeof(gr_complex)),
                        gr::io_signature::make (num_elements, num_elements, sizeof(gr_complex))),
        d_num_elements (num_elements),
        d_spacing (dist),
        d_lambda (lambda),
        d_array_conf (array_conf)
{
  d_positions.resize (d_num_elements);
  double lim;
  int index;
  switch (d_array_conf)
    {
    case 0: /* Linear array */
      lim = (d_num_elements - 1) / 2.0;
      index = 0;

      for (float i = -lim; i <= lim; i++) {
        d_positions[index].resize (2);
        d_positions[index][0] = i * d_spacing;
        index++;
      }
      break;
    case 1: /* Square array */
      int dimension = std::sqrt (d_num_elements);
      if (!dimension * dimension == d_num_elements) {
        printf ("Number of streams given not appropriate for square array\n");
        break;
      }
      lim = (dimension - 1) / 2.0;
      index = 0;
      for (float j = -lim; j <= lim; j++) {
        for (float i = -lim; i <= lim; i++) {
          d_positions[index].resize (2);
          d_positions[index][0] = i * d_spacing;
          d_positions[index][1] = j * d_spacing;
          index++;
        }
      }
      break;
    }

  d_port_in = pmt::mp ("coords_in_col");
  d_port_out = pmt::mp ("req_coordinates");
  message_port_register_in (d_port_in);
  message_port_register_out (d_port_out);
  d_azimuth = ang_az;
  d_elevation = ang_el;
  set_msg_handler (d_port_in,
                   boost::bind (&collector_impl::update_coordinates, this, _1));
}

/*
 * Our virtual destructor.
 */
collector_impl::~collector_impl ()
{
}

int
collector_impl::work (int noutput_items, gr_vector_const_void_star &input_items,
                      gr_vector_void_star &output_items)
{
  if (d_ack) {
    message_port_pub (d_port_out, pmt::from_bool (true));
    d_ack = false;
  }
  const gr_complex **in = (const gr_complex**) &input_items[0];
  gr_complex **out = (gr_complex**) &output_items[0];
  double d;
  for (int i = 0; i < d_num_elements; i++) {
    d = 2 * M_PI
        * (d_positions[i][1] * std::cos (d_elevation * M_PI / 180.0)
            * std::sin (d_azimuth * M_PI / 180.0)
            + d_positions[i][0] * std::cos (d_elevation * M_PI / 180.0)
                * std::cos (d_azimuth * M_PI / 180.0)
            + d_positions[i][2] * std::sin (d_elevation * M_PI / 180.0))
        / (d_lambda * 1.0);
    gr_complex f = gr_complex (std::cos (d), std::sin (d));
    for (int j = 0; j < noutput_items; j++) {
      out[i][j] = f * in[i][j];
    }
  }

  return noutput_items;
}

void
collector_impl::update_coordinates (pmt::pmt_t msg)
{
  d_ack = true;
  d_azimuth = pmt::to_double (pmt::tuple_ref (msg, 0));
  d_elevation = pmt::to_double (pmt::tuple_ref (msg, 1));
}

} /* namespace gsarray */
} /* namespace gr */

