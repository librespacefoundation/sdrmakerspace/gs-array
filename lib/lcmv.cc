/* -*- c++ -*- */
/*
 * Copyright 2019 gr-gsarray author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gsarray/lcmv.h>
#define ARMA_DONT_PRINT_ERRORS
namespace gr
{
namespace gsarray
{
using namespace arma;
lcmv::lcmv (size_t num_streams, uint8_t array_t, double spacing, double lambda) :
        algorithm (num_streams, array_t, spacing, lambda)
{
}

lcmv::~lcmv ()
{
}

size_t
lcmv::beamform (int noutput_items, gr_vector_const_void_star &input_items,
                gr_vector_void_star &output_items)
{

  std::complex<float> **streams = (std::complex<float>**) &input_items[0];
  std::complex<float> *out = (std::complex<float>*) output_items[0];
  cx_fmat fin;
  cx_fmat rxsig;

  for (int i = d_num_streams - 1; i >= 0; i--) {
    cx_fmat fin (&streams[i][0], 1, noutput_items, false, true);
    rxsig = join_cols (fin, rxsig);
  }
  cx_fvec steer_vec = cx_fvec (d_num_streams);
  for (int el = 0; el < d_num_streams; el++) {
    float d = 2 * M_PI
        * (d_positions[el][1] * std::cos (d_elevation * M_PI / 180.0)
            * std::sin (d_azimuth * M_PI / 180.0)
            + d_positions[el][0] * std::cos (d_elevation * M_PI / 180.0)
                * std::cos (d_azimuth * M_PI / 180.0)
            + 0 * std::sin (d_elevation * M_PI / 180.0)) / (d_lambda * 1.0);
    std::complex<float> s (std::cos (d), std::sin (d));
    steer_vec (el) = s;
  }

  cx_fmat w;
  double az_error = 0;
  double el_error = 0;

  cx_fvec steer_vec_e1;
  cx_fvec steer_vec_e2;
  cx_fvec steer_vec_e3;
  cx_fmat Rxx;
  cx_fvec resp;
  cx_fmat AS;
  for (int el = 0; el < d_num_streams; el++) {
    float d = 2 * M_PI
        * (d_positions[el][1]
            * std::cos ((d_elevation + el_error) * M_PI / 180.0)
            * std::sin ((d_azimuth + az_error) * M_PI / 180.0)
            + d_positions[el][0]
                * std::cos ((d_elevation + el_error) * M_PI / 180.0)
                * std::cos ((d_azimuth + az_error) * M_PI / 180.0)
            + 0 * std::sin ((d_elevation + el_error) * M_PI / 180.0))
        / (d_lambda * 1.0);
    std::complex<float> s (std::cos (d), std::sin (d));
    steer_vec (el) = s;
  }
  az_error = 1;
  el_error = 0;
  steer_vec_e1 = cx_fvec (d_num_streams);
  for (int el = 0; el < d_num_streams; el++) {
    float d = 2 * M_PI
        * (d_positions[el][1]
            * std::cos ((d_elevation + el_error) * M_PI / 180.0)
            * std::sin ((d_azimuth + az_error) * M_PI / 180.0)
            + d_positions[el][0]
                * std::cos ((d_elevation + el_error) * M_PI / 180.0)
                * std::cos ((d_azimuth + az_error) * M_PI / 180.0)
            + 0 * std::sin ((d_elevation + el_error) * M_PI / 180.0))
        / (d_lambda * 1.0);
    std::complex<float> s (std::cos (d), std::sin (d));
    steer_vec_e1 (el) = s;
  }
  az_error = 1;
  el_error = 1;
  steer_vec_e2 = cx_fvec (d_num_streams);
  for (int el = 0; el < d_num_streams; el++) {
    float d = 2 * M_PI
        * (d_positions[el][1]
            * std::cos ((d_elevation + el_error) * M_PI / 180.0)
            * std::sin ((d_azimuth + az_error) * M_PI / 180.0)
            + d_positions[el][0]
                * std::cos ((d_elevation + el_error) * M_PI / 180.0)
                * std::cos ((d_azimuth + az_error) * M_PI / 180.0)
            + 0 * std::sin ((d_elevation + el_error) * M_PI / 180.0))
        / (d_lambda * 1.0);
    std::complex<float> s (std::cos (d), std::sin (d));
    steer_vec_e2 (el) = s;
  }
  az_error = 0;
  el_error = 1;
  steer_vec_e3 = cx_fvec (d_num_streams);
  for (int el = 0; el < d_num_streams; el++) {
    float d = 2 * M_PI
        * (d_positions[el][1]
            * std::cos ((d_elevation + el_error) * M_PI / 180.0)
            * std::sin ((d_azimuth + az_error) * M_PI / 180.0)
            + d_positions[el][0]
                * std::cos ((d_elevation + el_error) * M_PI / 180.0)
                * std::cos ((d_azimuth + az_error) * M_PI / 180.0)
            + 0 * std::sin ((d_elevation + el_error) * M_PI / 180.0))
        / (d_lambda * 1.0);
    std::complex<float> s (std::cos (d), std::sin (d));
    steer_vec_e3 (el) = s;
  }

  AS = join_rows (steer_vec, steer_vec_e1);
  AS = join_rows (AS, steer_vec_e2);
  AS = join_rows (AS, steer_vec_e3);
  try {
    Rxx = (rxsig * rxsig.t ()) / noutput_items;
    cx_fmat L = solve (Rxx, AS);
    cx_fmat T = AS.t () * L;
    cx_fmat P = solve (T, d_resp);
    w = L * P;
  }
  catch (const std::exception &e) {
    std::cout << e.what ();
    return 0;
  }

  cx_fmat o = rxsig.st () * conj (w);

  memcpy (out, (gr_complex*) o.memptr (), noutput_items * sizeof(gr_complex));
  return noutput_items;
}

} /* namespace gsarray */
} /* namespace gr */

