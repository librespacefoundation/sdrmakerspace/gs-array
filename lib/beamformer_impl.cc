#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "beamformer_impl.h"

namespace gr
{
namespace gsarray
{
using namespace arma;

beamformer::sptr
beamformer::make (size_t num_streams, uint8_t array_t, double spacing,
                  double lambda, uint8_t method)
{
  return gnuradio::get_initial_sptr (
      new beamformer_impl (num_streams, array_t, spacing, lambda, method));
}

/*
 * The private constructor
 */
beamformer_impl::beamformer_impl (size_t num_streams, uint8_t array_t,
                                  double spacing, double lambda, uint8_t method) :
        gr::sync_block (
            "beamformer",
            gr::io_signature::make (num_streams, num_streams,
                                    sizeof(std::complex<float>)),
            gr::io_signature::make (1, 1, sizeof(std::complex<float>))),
        d_num_streams (num_streams),
        d_array_t (array_t),
        d_spacing (spacing),
        d_lambda (lambda),
        d_method (method),
        d_azimuth (0),
        d_elevation (0),
        d_coord_in (pmt::mp ("coordinates"))
{
  //set_max_noutput_items (2048);


  switch(d_method){
    case LCMV:
      d_beamformer = new lcmv (num_streams, array_t, spacing, lambda);
      break;
    case PS:
      d_beamformer = new phaseshift (num_streams, array_t, spacing, lambda);
      break;
  }
  /* Message port and their callback functions declaration*/
  message_port_register_in (d_coord_in);

  set_msg_handler (
      d_coord_in, boost::bind (&beamformer_impl::update_coordinates, this, _1));
}

beamformer_impl::~beamformer_impl ()
{
}

int
beamformer_impl::work (int noutput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
{
  size_t ret = d_beamformer->beamform (noutput_items, input_items,
                                       output_items);

  return ret;
}

void
beamformer_impl::update_coordinates (pmt::pmt_t msg)
{
  d_azimuth = pmt::to_double (pmt::tuple_ref (msg, 0));
  d_elevation = pmt::to_double (pmt::tuple_ref (msg, 1));
  d_beamformer->update_coordinates (d_azimuth, d_elevation);
}

} /* namespace gsarray */
} /* namespace gr */

