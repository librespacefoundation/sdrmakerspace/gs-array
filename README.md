# gs-array

This repository implements the Phaseshift and LCMV beamformers for GNURadio

For the simulation example flowgraph beamformer_sim.grc to run you will need
a custom version of the gr-leo module in https://gitlab.com/Vardakis/gr-leo/tree/get_coord