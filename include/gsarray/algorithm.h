/* -*- c++ -*- */
/*
 * Copyright 2019 gr-gsarray author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_GSARRAY_ALGORITHM_H
#define INCLUDED_GSARRAY_ALGORITHM_H
#define ARMA_DONT_PRINT_ERRORS

#include <gsarray/api.h>
#include <vector>
#include <armadillo>

namespace gr
{
namespace gsarray
{

class GSARRAY_API algorithm
{
public:
  algorithm (size_t num_streams, uint8_t array_t, double spacing,
             double lambda);
  virtual ~algorithm ();

  virtual
  size_t beamform(int noutput_items,
                  gr_vector_const_void_star &input_items,
                  gr_vector_void_star &output_items) ;

  void
  update_coordinates(double azimuth, double elevation);

  size_t d_num_streams;
  uint8_t d_array_t;
  double d_azimuth;
  double d_elevation;
  double d_spacing;
  double d_lambda;
  std::vector<std::vector<double>> d_positions;
  arma::cx_fvec d_resp;
};

} // namespace gsarray
} // namespace gr

#endif /* INCLUDED_GSARRAY_ALGORITHM_H */

