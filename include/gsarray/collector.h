

#ifndef INCLUDED_GSARRAY_COLLECTOR_H
#define INCLUDED_GSARRAY_COLLECTOR_H

#include <gsarray/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace gsarray {

    /*!
     * \brief <+description of block+>
     * \ingroup gsarray
     *
     */
    class GSARRAY_API collector : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<collector> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of gsarray::collector.
       *
       * To avoid accidental use of raw pointers, gsarray::collector's
       * constructor is in a private implementation
       * class. gsarray::collector::make is the public interface for
       * creating new instances.
       */
      static sptr make(uint16_t num_elements, float dist, float lambda, uint8_t array_conf,
                       uint8_t input_ang_src, float ang_az, float ang_el);
    };

  } // namespace gsarray
} // namespace gr

#endif /* INCLUDED_GSARRAY_COLLECTOR_H */

