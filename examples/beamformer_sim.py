#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Beamformer Simulation
# GNU Radio version: 3.8.0.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
import numpy
from gnuradio import digital
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import gsarray
import leo
from gnuradio import qtgui

class beamformer_sim(gr.top_block, Qt.QWidget):

    def __init__(self, nf=-80):
        gr.top_block.__init__(self, "Beamformer Simulation")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Beamformer Simulation")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "beamformer_sim")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Parameters
        ##################################################
        self.nf = nf

        ##################################################
        # Variables
        ##################################################
        self.satellite_tx_antenna = satellite_tx_antenna = leo.dipole_antenna_make(5, 435.769e6, 3, 0)
        self.satellite_rx_antenna = satellite_rx_antenna = leo.dipole_antenna_make(5, 435.765e6, 3, 0)
        self.variable_satellite_0 = variable_satellite_0 = leo.satellite_make('UPSAT', '1 25544U 98067A   18268.52547184  .00016717  00000-0  10270-3 0  9019', '2 25544  51.6373 238.6885 0003885 206.9748 153.1203 15.53729445 14114', 435e6, 435e6, satellite_tx_antenna, satellite_rx_antenna)
        self.tracker_tx_antenna = tracker_tx_antenna = leo.yagi_antenna_make(0,435.765e6, 0, 0, 2.35)
        self.tracker_rx_antenna = tracker_rx_antenna = leo.yagi_antenna_make(0,435.769e6, 2, 0, 0)
        self.fc_alg = fc_alg = 435e6
        self.fc = fc = 435e6
        self.c = c = 299792458
        self.variable_tracker_0 = variable_tracker_0 = leo.tracker_make(variable_satellite_0, 35.3333, 25.1833, 1, '2018-09-25T15:48:25.0000000Z', '2018-09-25T15:58:35.0000000Z', 1000, 435E6, 435e6, tracker_tx_antenna, tracker_rx_antenna)
        self.l_alg = l_alg = c/fc_alg
        self.l = l = c/fc
        self.variable_leo_model_def_5 = variable_leo_model_def_5 = leo.leo_model_make(variable_tracker_0, 1, 5,
          													0, 0,
          													2, 4,
          													7.5, 20, 90)
        self.variable_leo_model_def_3 = variable_leo_model_def_3 = leo.leo_model_make(variable_tracker_0, 1, 5,
          													0, 0,
          													2, 4,
          													7.5, 20, 90)
        self.variable_leo_model_def_2 = variable_leo_model_def_2 = leo.leo_model_make(variable_tracker_0, 1, 5,
          													0, 0,
          													2, 4,
          													7.5, 20, 90)
        self.variable_leo_model_def_1 = variable_leo_model_def_1 = leo.leo_model_make(variable_tracker_0, 1, 5,
          													0, 0,
          													2, 4,
          													7.5, 20, 90)
        self.variable_leo_model_def_0 = variable_leo_model_def_0 = leo.leo_model_make(variable_tracker_0, 1, 5,
          													0, 0,
          													2, 4,
          													7.5, 20, 90)
        self.variable_constellation_1 = variable_constellation_1 = digital.constellation_bpsk().base()
        self.variable_constellation_0_0 = variable_constellation_0_0 = digital.constellation_bpsk().base()
        self.variable_constellation_0 = variable_constellation_0 = digital.constellation_bpsk().base()
        self.spacing_alg = spacing_alg = l_alg/2.0
        self.spacing = spacing = l/2.0
        self.samp_rate_tx = samp_rate_tx = 1e6
        self.num_elements = num_elements = 4
        self.ampl = ampl = 100e3

        ##################################################
        # Blocks
        ##################################################
        self.qtgui_freq_sink_x_1 = qtgui.freq_sink_c(
            1024, #size
            firdes.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate_tx, #bw
            "", #name
            2
        )
        self.qtgui_freq_sink_x_1.set_update_time(0.10)
        self.qtgui_freq_sink_x_1.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_1.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_1.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_1.enable_autoscale(False)
        self.qtgui_freq_sink_x_1.enable_grid(False)
        self.qtgui_freq_sink_x_1.set_fft_average(1.0)
        self.qtgui_freq_sink_x_1.enable_axis_labels(True)
        self.qtgui_freq_sink_x_1.enable_control_panel(False)



        labels = ['One Antenna', 'Beamformed Signal', '', '', '',
            '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
            "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(2):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_1.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_1.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_1.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_1.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_1.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_1_win = sip.wrapinstance(self.qtgui_freq_sink_x_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_freq_sink_x_1_win)
        self.leo_channel_model_0_0_0_0 = leo.channel_model(samp_rate_tx, variable_leo_model_def_3, 1, nf, 1)
        self.leo_channel_model_0_0_0 = leo.channel_model(samp_rate_tx, variable_leo_model_def_2, 1, nf, 1)
        self.leo_channel_model_0_0 = leo.channel_model(samp_rate_tx, variable_leo_model_def_1, 1, nf, 1)
        self.leo_channel_model_0 = leo.channel_model(samp_rate_tx, variable_leo_model_def_0, 1, nf, 1)
        self.gsarray_collector_0 = gsarray.collector(num_elements, spacing, l, 1, 0, 0, 0)
        self.gsarray_beamformer_0_0 = gsarray.beamformer(4,1,spacing,l,0)
        self.digital_psk_mod_0 = digital.psk.psk_mod(
            constellation_points=2,
            mod_code="gray",
            differential=True,
            samples_per_symbol=8,
            excess_bw=0.35,
            verbose=False,
            log=False)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_cc(ampl)
        self.analog_random_source_x_0 = blocks.vector_source_b(list(map(int, numpy.random.randint(0, 256, 100))), True)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.gsarray_collector_0, 'req_coordinates'), (self.leo_channel_model_0_0_0_0, 'coord_req_in'))
        self.msg_connect((self.leo_channel_model_0_0_0_0, 'coord_supply'), (self.gsarray_beamformer_0_0, 'coordinates'))
        self.msg_connect((self.leo_channel_model_0_0_0_0, 'coord_supply'), (self.gsarray_collector_0, 'coords_in_col'))
        self.connect((self.analog_random_source_x_0, 0), (self.digital_psk_mod_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.gsarray_collector_0, 1))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.gsarray_collector_0, 3))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.gsarray_collector_0, 2))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.gsarray_collector_0, 0))
        self.connect((self.digital_psk_mod_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.gsarray_beamformer_0_0, 0), (self.qtgui_freq_sink_x_1, 1))
        self.connect((self.gsarray_collector_0, 0), (self.leo_channel_model_0, 0))
        self.connect((self.gsarray_collector_0, 1), (self.leo_channel_model_0_0, 0))
        self.connect((self.gsarray_collector_0, 2), (self.leo_channel_model_0_0_0, 0))
        self.connect((self.gsarray_collector_0, 3), (self.leo_channel_model_0_0_0_0, 0))
        self.connect((self.leo_channel_model_0, 0), (self.gsarray_beamformer_0_0, 0))
        self.connect((self.leo_channel_model_0, 0), (self.qtgui_freq_sink_x_1, 0))
        self.connect((self.leo_channel_model_0_0, 0), (self.gsarray_beamformer_0_0, 1))
        self.connect((self.leo_channel_model_0_0_0, 0), (self.gsarray_beamformer_0_0, 2))
        self.connect((self.leo_channel_model_0_0_0_0, 0), (self.gsarray_beamformer_0_0, 3))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "beamformer_sim")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_nf(self):
        return self.nf

    def set_nf(self, nf):
        self.nf = nf

    def get_satellite_tx_antenna(self):
        return self.satellite_tx_antenna

    def set_satellite_tx_antenna(self, satellite_tx_antenna):
        self.satellite_tx_antenna = satellite_tx_antenna
        self.satellite_tx_antenna.set_pointing_error(0)

    def get_satellite_rx_antenna(self):
        return self.satellite_rx_antenna

    def set_satellite_rx_antenna(self, satellite_rx_antenna):
        self.satellite_rx_antenna = satellite_rx_antenna
        self.satellite_rx_antenna.set_pointing_error(0)

    def get_variable_satellite_0(self):
        return self.variable_satellite_0

    def set_variable_satellite_0(self, variable_satellite_0):
        self.variable_satellite_0 = variable_satellite_0

    def get_tracker_tx_antenna(self):
        return self.tracker_tx_antenna

    def set_tracker_tx_antenna(self, tracker_tx_antenna):
        self.tracker_tx_antenna = tracker_tx_antenna
        self.tracker_tx_antenna.set_pointing_error(0)

    def get_tracker_rx_antenna(self):
        return self.tracker_rx_antenna

    def set_tracker_rx_antenna(self, tracker_rx_antenna):
        self.tracker_rx_antenna = tracker_rx_antenna
        self.tracker_rx_antenna.set_pointing_error(0)

    def get_fc_alg(self):
        return self.fc_alg

    def set_fc_alg(self, fc_alg):
        self.fc_alg = fc_alg
        self.set_l_alg(self.c/self.fc_alg)

    def get_fc(self):
        return self.fc

    def set_fc(self, fc):
        self.fc = fc
        self.set_l(self.c/self.fc)

    def get_c(self):
        return self.c

    def set_c(self, c):
        self.c = c
        self.set_l(self.c/self.fc)
        self.set_l_alg(self.c/self.fc_alg)

    def get_variable_tracker_0(self):
        return self.variable_tracker_0

    def set_variable_tracker_0(self, variable_tracker_0):
        self.variable_tracker_0 = variable_tracker_0

    def get_l_alg(self):
        return self.l_alg

    def set_l_alg(self, l_alg):
        self.l_alg = l_alg
        self.set_spacing_alg(self.l_alg/2.0)

    def get_l(self):
        return self.l

    def set_l(self, l):
        self.l = l
        self.set_spacing(self.l/2.0)

    def get_variable_leo_model_def_5(self):
        return self.variable_leo_model_def_5

    def set_variable_leo_model_def_5(self, variable_leo_model_def_5):
        self.variable_leo_model_def_5 = variable_leo_model_def_5

    def get_variable_leo_model_def_3(self):
        return self.variable_leo_model_def_3

    def set_variable_leo_model_def_3(self, variable_leo_model_def_3):
        self.variable_leo_model_def_3 = variable_leo_model_def_3

    def get_variable_leo_model_def_2(self):
        return self.variable_leo_model_def_2

    def set_variable_leo_model_def_2(self, variable_leo_model_def_2):
        self.variable_leo_model_def_2 = variable_leo_model_def_2

    def get_variable_leo_model_def_1(self):
        return self.variable_leo_model_def_1

    def set_variable_leo_model_def_1(self, variable_leo_model_def_1):
        self.variable_leo_model_def_1 = variable_leo_model_def_1

    def get_variable_leo_model_def_0(self):
        return self.variable_leo_model_def_0

    def set_variable_leo_model_def_0(self, variable_leo_model_def_0):
        self.variable_leo_model_def_0 = variable_leo_model_def_0

    def get_variable_constellation_1(self):
        return self.variable_constellation_1

    def set_variable_constellation_1(self, variable_constellation_1):
        self.variable_constellation_1 = variable_constellation_1

    def get_variable_constellation_0_0(self):
        return self.variable_constellation_0_0

    def set_variable_constellation_0_0(self, variable_constellation_0_0):
        self.variable_constellation_0_0 = variable_constellation_0_0

    def get_variable_constellation_0(self):
        return self.variable_constellation_0

    def set_variable_constellation_0(self, variable_constellation_0):
        self.variable_constellation_0 = variable_constellation_0

    def get_spacing_alg(self):
        return self.spacing_alg

    def set_spacing_alg(self, spacing_alg):
        self.spacing_alg = spacing_alg

    def get_spacing(self):
        return self.spacing

    def set_spacing(self, spacing):
        self.spacing = spacing

    def get_samp_rate_tx(self):
        return self.samp_rate_tx

    def set_samp_rate_tx(self, samp_rate_tx):
        self.samp_rate_tx = samp_rate_tx
        self.qtgui_freq_sink_x_1.set_frequency_range(0, self.samp_rate_tx)

    def get_num_elements(self):
        return self.num_elements

    def set_num_elements(self, num_elements):
        self.num_elements = num_elements

    def get_ampl(self):
        return self.ampl

    def set_ampl(self, ampl):
        self.ampl = ampl
        self.blocks_multiply_const_vxx_0.set_k(self.ampl)


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "--nf", dest="nf", type=intx, default=-80,
        help="Set nf [default=%(default)r]")
    return parser


def main(top_block_cls=beamformer_sim, options=None):
    if options is None:
        options = argument_parser().parse_args()

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(nf=options.nf)
    tb.start()
    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()
    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
