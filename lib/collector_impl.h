
#ifndef INCLUDED_GSARRAY_COLLECTOR_IMPL_H
#define INCLUDED_GSARRAY_COLLECTOR_IMPL_H

#include <gsarray/collector.h>
#include <vector>

namespace gr {
  namespace gsarray {

    class collector_impl : public collector
    {
     private:
      float d_spacing;
      float d_lambda;
      double d_azimuth;
      double d_elevation;
      pmt::pmt_t d_port_in;
      pmt::pmt_t d_port_out;
      std::vector<gr_complex>steer_vec[4];
      bool d_ack = true;
      uint8_t d_array_conf;
      uint16_t d_num_elements;
      std::vector<std::vector<double>> d_positions;

      void
      update_coordinates(pmt::pmt_t);

     public:
      collector_impl(uint16_t num_elements, float dist, float lambda, uint8_t array_conf,
                     uint8_t input_ang_src, float ang_az, float ang_el);
      ~collector_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace gsarray
} // namespace gr

#endif /* INCLUDED_GSARRAY_COLLECTOR_IMPL_H */

